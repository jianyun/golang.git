package main

import (
	"bufio"
	"bytes"
	"compress/zlib"
	"encoding/base64"
	"fmt"
	"io"
	"os"
)

func encoding(json bytes.Buffer) (bytes.Buffer, error) {
	var compressed bytes.Buffer
	encoder := base64.NewEncoder(base64.URLEncoding, &compressed)
	compressor := zlib.NewWriter(encoder)
	if _, err := json.WriteTo(compressor); err != nil {
		return bytes.Buffer{}, err
	}
	if err := compressor.Close(); err != nil {
		return bytes.Buffer{}, err
	}
	if err := encoder.Close(); err != nil {
		return bytes.Buffer{}, err
	}
	return compressed, nil
}

func decoding(url []byte) ([]byte, error) {
	b := bytes.NewReader(url)
	decoder := base64.NewDecoder(base64.URLEncoding, b)
	compressor, err := zlib.NewReader(decoder)
	if err != nil {
		fmt.Printf("err = %s\n", err)
	}
	var out bytes.Buffer
	io.Copy(&out, compressor)
	if err := compressor.Close(); err != nil {
		return nil, err
	}
	return out.Bytes(), nil
}

/**
  http://localhost:8089/distsqlplan/decode.html#eJzkl2tr40YUhv_KMF_W2R3bc-YuQUHpDUKzTsmFUtoQlGhwxNqSkWS6Ycl_L5LvI1m2U6fZZb-N5vrOec95JH3BSRrZQTi2Ofb_woAJZphgjgkW-JbgSZY-2DxPs3J4Nvks-ox9SnCcTKZF2X1L8EOaWex_wUVcjCz28XV4P7KXNoxs1qeY4MgWYTyqjphk8TjMnoJRnNi4sGNM8NUkTHIfdftdjzHOJUgmOGVaKSkZCpMICZQWjzbLMcG_xqPCZj4KANDfU0r5ww_oHXie6VLTpfKd7_s_n17_ggm-tElUzVTvO-D7_tng2nQDfUJQx-06ed8JzIdFzwlBgSQoUAQFmqDAIyiA8hpZ-k-OMhtGPuKeAKokJjgvwtEIFfHY-kj0PCOVkqXQ-6fCLmZLRtHH-EdcRmrUs8kwTmxvNoSKbGrLbeLkkz97uH0mOJ0Wq_DmRTi02Idnsr8Fp8NhZodhkWZ9vulAoEigMcGngz_vBhfXd4Ob8_NOoE5qXbrsurr52An4siWWLVi2WNn66eJmcD2fOW-v5sq13lX77vLij6vOiRNYI4SnPSewIHrceJ70ysiOw89obMdp9oSmuY18BBT9VkV3axBXcbl_Qo9h_uiEBEjA8O3zKtZsa6xXW02TNItsZqONzapdWtxQTUe7oYe6G6zVDblsqXnr7qyMtt54MstZ3kY_0M3H6vhl_QBBASMo4AQFYq02eL8qD9EPTFkl_QBoWSngGOpaqXvMKMFMm5XbSoAfUgJXaVbYrO-5Af9AAvYBE3wej-MCOXKhUa6kslEuX2ReORLF-ad5P0Xb7yA27gD7kxT2JWkjSF28MioFE18pXkEaZsDFK_S4osCUi1dGzdHxusOXtYIW3xJeQQphtHYCK3tMC63529AVjkhX_Z3RlTbiykAzXduwxA9J_zlagR7E1i1ihdgq9j-Ale0PVvYysFIQYBjdACvzPKM4U7QCK8zA-vVwtSxyXvtqZboHcuxSVfGjQ3WHJ2t1LL8lqAolPeMyFXpKA4B-G6ayIzLVfGdMrf18VJhiXquV-2F1RwEssAoHYXXLF7Zmr_TJyhtzqymzlsobU2yQdtNJHxz6Lq64TYvcqqXhiEubT9Ikt06-N-9MS5E2GtrZxfJ0mj3Y37P0ofrTmD1eVOuqjsjmxWwUZg9nyWyoFLi-GNzFsL6YbSyGSk0lbG5xOi3s_L-k_opssLEM3CHnq_3Op3ufv3iL5DYpfMS5OlQR0P0k8RdK0kzXJbFWh3m7w7w9PbZbvC4LmOJATV2aaN1dtkuT_2_yOeZTSkVDRraLOnpGHnj-6-efqEtSrSbrdpP1MfKPCcHrukzr1l67Lu8tk88YRln9Qu2aXp2G8lBFx8_GOo53vN52vd9qOH9R-mnmNUhr5zK4YL59_jcAAP__aBFoQg==

  https://cockroachdb.github.io/distsqlplan/decode.html#eJzkl2tr40YUhv_KMF_W2R3bc-YuQUHpDUKzTsmFUtoQlGhwxNqSkWS6Ycl_L5LvI1m2U6fZZb-N5vrOec95JH3BSRrZQTi2Ofb_woAJZphgjgkW-JbgSZY-2DxPs3J4Nvks-ox9SnCcTKZF2X1L8EOaWex_wUVcjCz28XV4P7KXNoxs1qeY4MgWYTyqjphk8TjMnoJRnNi4sGNM8NUkTHIfdftdjzHOJUgmOGVaKSkZCpMICZQWjzbLMcG_xqPCZj4KANDfU0r5ww_oHXie6VLTpfKd7_s_n17_ggm-tElUzVTvO-D7_tng2nQDfUJQx-06ed8JzIdFzwlBgSQoUAQFmqDAIyiA8hpZ-k-OMhtGPuKeAKokJjgvwtEIFfHY-kj0PCOVkqXQ-6fCLmZLRtHH-EdcRmrUs8kwTmxvNoSKbGrLbeLkkz97uH0mOJ0Wq_DmRTi02Idnsr8Fp8NhZodhkWZ9vulAoEigMcGngz_vBhfXd4Ob8_NOoE5qXbrsurr52An4siWWLVi2WNn66eJmcD2fOW-v5sq13lX77vLij6vOiRNYI4SnPSewIHrceJ70ysiOw89obMdp9oSmuY18BBT9VkV3axBXcbl_Qo9h_uiEBEjA8O3zKtZsa6xXW02TNItsZqONzapdWtxQTUe7oYe6G6zVDblsqXnr7qyMtt54MstZ3kY_0M3H6vhl_QBBASMo4AQFYq02eL8qD9EPTFkl_QBoWSngGOpaqXvMKMFMm5XbSoAfUgJXaVbYrO-5Af9AAvYBE3wej-MCOXKhUa6kslEuX2ReORLF-ad5P0Xb7yA27gD7kxT2JWkjSF28MioFE18pXkEaZsDFK_S4osCUi1dGzdHxusOXtYIW3xJeQQphtHYCK3tMC63529AVjkhX_Z3RlTbiykAzXduwxA9J_zlagR7E1i1ihdgq9j-Ale0PVvYysFIQYBjdACvzPKM4U7QCK8zA-vVwtSxyXvtqZboHcuxSVfGjQ3WHJ2t1LL8lqAolPeMyFXpKA4B-G6ayIzLVfGdMrf18VJhiXquV-2F1RwEssAoHYXXLF7Zmr_TJyhtzqymzlsobU2yQdtNJHxz6Lq64TYvcqqXhiEubT9Ikt06-N-9MS5E2GtrZxfJ0mj3Y37P0ofrTmD1eVOuqjsjmxWwUZg9nyWyoFLi-GNzFsL6YbSyGSk0lbG5xOi3s_L-k_opssLEM3CHnq_3Op3ufv3iL5DYpfMS5OlQR0P0k8RdK0kzXJbFWh3m7w7w9PbZbvC4LmOJATV2aaN1dtkuT_2_yOeZTSkVDRraLOnpGHnj-6-efqEtSrSbrdpP1MfKPCcHrukzr1l67Lu8tk88YRln9Qu2aXp2G8lBFx8_GOo53vN52vd9qOH9R-mnmNUhr5zK4YL59_jcAAP__aBFoQg==
*/
func main() {
	reader := bufio.NewReader(os.Stdin)
	//json转url
	fmt.Printf("请输入json串：如果不需要可输入回车\n")
	json, _ := reader.ReadString('\n')
	if len(json) > 1 {
		fmt.Printf("%s", json)
		//json1 := ""
		json = "{\"nodeNames\":[\"1\",\"2\",\"3\",\"4\"],\"processors\":[{\"nodeIdx\":0,\"inputs\":[],\"core\":{\"title\":\"TableReader/0\",\"details\":[\"primary@lineitem\",\"Spans: -/-9223351524300698829\",\"Filter: @11 \\u003c= '1998-08-05':::DATE\",\"Render: @6*(1:::INT8-@7), (@6*(1:::INT8-@7))*(@8+1:::INT8), @5, @6, @7, @9, @10\",\"batches output: 2630\",\"tuples output: 2565548\",\"IO time: 13.875285s\",\"col.engine.read: true\",\"sink:true\"]},\"outputs\":[],\"stage\":1},{\"nodeIdx\":0,\"inputs\":[],\"core\":{\"title\":\"Aggregator/3\",\"details\":[\"@6,@7\",\"ANY_NOT_NULL(@6)\",\"ANY_NOT_NULL(@7)\",\"SUM(@3)\",\"SUM(@4)\",\"SUM(@1)\",\"SUM(@2)\",\"COUNT(@3)\",\"COUNT(@4)\",\"SUM(@5)\",\"COUNT(@5)\",\"COUNT_ROWS()\",\"batches output: 1\",\"tuples output: 4\",\"execution time: 2.059788s\",\"max vectorized memory allocated: 1.7 MiB\",\"sink:true\"]},\"outputs\":[{\"title\":\"by hash\",\"details\":[\"@1,@2\"]}],\"stage\":2},{\"nodeIdx\":0,\"inputs\":[{\"title\":\"unordered\",\"details\":[]}],\"core\":{\"title\":\"Aggregator/6\",\"details\":[\"@1,@2\",\"ANY_NOT_NULL(@1)\",\"ANY_NOT_NULL(@2)\",\"SUM(@3)\",\"SUM(@4)\",\"SUM(@5)\",\"SUM(@6)\",\"SUM_INT(@7)\",\"SUM_INT(@8)\",\"SUM(@9)\",\"SUM_INT(@10)\",\"SUM_INT(@11)\",\"Render: @1, @2, @3, @4, @5, @6, @3/@7, @4/@8, @9/@10, @11\",\"batches output: 1\",\"tuples output: 1\",\"execution time: 417µs\",\"max vectorized memory allocated: 550 KiB\"]},\"outputs\":[],\"stage\":3},{\"nodeIdx\":0,\"inputs\":[],\"core\":{\"title\":\"Sorter/9\",\"details\":[\"@1+,@2+\",\"Limit 1\",\"batches output: 1\",\"tuples output: 1\",\"execution time: 13µs\",\"max vectorized memory allocated: 10 KiB\"]},\"outputs\":[],\"stage\":4},{\"nodeIdx\":1,\"inputs\":[],\"core\":{\"title\":\"TableReader/2\",\"details\":[\"primary@lineitem\",\"Spans: /-9223351524298155012-\",\"Filter: @11 \\u003c= '1998-08-05':::DATE\",\"Render: @6*(1:::INT8-@7), (@6*(1:::INT8-@7))*(@8+1:::INT8), @5, @6, @7, @9, @10\",\"batches output: 828\",\"tuples output: 807879\",\"IO time: 5.594771s\",\"col.engine.read: true\",\"sink:true\"]},\"outputs\":[],\"stage\":1},{\"nodeIdx\":1,\"inputs\":[],\"core\":{\"title\":\"Aggregator/5\",\"details\":[\"@6,@7\",\"ANY_NOT_NULL(@6)\",\"ANY_NOT_NULL(@7)\",\"SUM(@3)\",\"SUM(@4)\",\"SUM(@1)\",\"SUM(@2)\",\"COUNT(@3)\",\"COUNT(@4)\",\"SUM(@5)\",\"COUNT(@5)\",\"COUNT_ROWS()\",\"batches output: 1\",\"tuples output: 4\",\"execution time: 716.081ms\",\"max vectorized memory allocated: 1.7 MiB\",\"sink:true\"]},\"outputs\":[{\"title\":\"by hash\",\"details\":[\"@1,@2\"]}],\"stage\":2},{\"nodeIdx\":1,\"inputs\":[{\"title\":\"unordered\",\"details\":[]}],\"core\":{\"title\":\"Aggregator/8\",\"details\":[\"@1,@2\",\"ANY_NOT_NULL(@1)\",\"ANY_NOT_NULL(@2)\",\"SUM(@3)\",\"SUM(@4)\",\"SUM(@5)\",\"SUM(@6)\",\"SUM_INT(@7)\",\"SUM_INT(@8)\",\"SUM(@9)\",\"SUM_INT(@10)\",\"SUM_INT(@11)\",\"Render: @1, @2, @3, @4, @5, @6, @3/@7, @4/@8, @9/@10, @11\",\"batches output: 0\",\"tuples output: 0\",\"execution time: 82µs\",\"max vectorized memory allocated: 430 KiB\"]},\"outputs\":[],\"stage\":3},{\"nodeIdx\":1,\"inputs\":[],\"core\":{\"title\":\"Sorter/11\",\"details\":[\"@1+,@2+\",\"Limit 1\",\"batches output: 0\",\"tuples output: 0\",\"execution time: 2µs\"]},\"outputs\":[],\"stage\":4},{\"nodeIdx\":2,\"inputs\":[],\"core\":{\"title\":\"TableReader/1\",\"details\":[\"primary@lineitem\",\"Spans: /-9223351524300698829-/-9223351524298155012\",\"Filter: @11 \\u003c= '1998-08-05':::DATE\",\"Render: @6*(1:::INT8-@7), (@6*(1:::INT8-@7))*(@8+1:::INT8), @5, @6, @7, @9, @10\",\"batches output: 2544\",\"tuples output: 2482416\",\"IO time: 13.405612s\",\"col.engine.read: true\",\"sink:true\"]},\"outputs\":[],\"stage\":1},{\"nodeIdx\":2,\"inputs\":[],\"core\":{\"title\":\"Aggregator/4\",\"details\":[\"@6,@7\",\"ANY_NOT_NULL(@6)\",\"ANY_NOT_NULL(@7)\",\"SUM(@3)\",\"SUM(@4)\",\"SUM(@1)\",\"SUM(@2)\",\"COUNT(@3)\",\"COUNT(@4)\",\"SUM(@5)\",\"COUNT(@5)\",\"COUNT_ROWS()\",\"batches output: 1\",\"tuples output: 4\",\"execution time: 2.016141s\",\"max vectorized memory allocated: 1.7 MiB\",\"sink:true\"]},\"outputs\":[{\"title\":\"by hash\",\"details\":[\"@1,@2\"]}],\"stage\":2},{\"nodeIdx\":2,\"inputs\":[{\"title\":\"unordered\",\"details\":[]}],\"core\":{\"title\":\"Aggregator/7\",\"details\":[\"@1,@2\",\"ANY_NOT_NULL(@1)\",\"ANY_NOT_NULL(@2)\",\"SUM(@3)\",\"SUM(@4)\",\"SUM(@5)\",\"SUM(@6)\",\"SUM_INT(@7)\",\"SUM_INT(@8)\",\"SUM(@9)\",\"SUM_INT(@10)\",\"SUM_INT(@11)\",\"Render: @1, @2, @3, @4, @5, @6, @3/@7, @4/@8, @9/@10, @11\",\"batches output: 1\",\"tuples output: 3\",\"execution time: 517µs\",\"max vectorized memory allocated: 550 KiB\"]},\"outputs\":[],\"stage\":3},{\"nodeIdx\":2,\"inputs\":[],\"core\":{\"title\":\"Sorter/10\",\"details\":[\"@1+,@2+\",\"Limit 1\",\"batches output: 1\",\"tuples output: 1\",\"execution time: 12µs\",\"max vectorized memory allocated: 10 KiB\"]},\"outputs\":[],\"stage\":4},{\"nodeIdx\":3,\"inputs\":[{\"title\":\"ordered\",\"details\":[\"@1+,@2+\"]}],\"core\":{\"title\":\"No-op/12\",\"details\":[\"Limit 1\",\"batches output: 1\",\"tuples output: 1\",\"execution time: 1µs\"]},\"outputs\":[],\"stage\":5},{\"nodeIdx\":3,\"inputs\":[],\"core\":{\"title\":\"Response\",\"details\":[]},\"outputs\":[],\"stage\":0}],\"edges\":[{\"sourceProc\":0,\"sourceOutput\":0,\"destProc\":1,\"destInput\":0},{\"sourceProc\":1,\"sourceOutput\":1,\"destProc\":2,\"destInput\":1,\"stats\":[\"batches output: 1\",\"tuples output: 1\",\"execution time: 15.936317s\",\"max vectorized memory allocated: 2.1 MiB\"]},{\"sourceProc\":1,\"sourceOutput\":1,\"destProc\":10,\"destInput\":1,\"stats\":[\"batches output: 1\",\"tuples output: 3\",\"execution time: 15.936682s\"]},{\"sourceProc\":1,\"sourceOutput\":1,\"destProc\":6,\"destInput\":1,\"stats\":[\"batches output: 0\",\"tuples output: 0\",\"execution time: 15.936787s\"]},{\"sourceProc\":2,\"sourceOutput\":0,\"destProc\":3,\"destInput\":0},{\"sourceProc\":3,\"sourceOutput\":0,\"destProc\":12,\"destInput\":1,\"stats\":[\"batches output: 1\",\"tuples output: 1\",\"execution time: 15.937516s\"]},{\"sourceProc\":4,\"sourceOutput\":0,\"destProc\":5,\"destInput\":0},{\"sourceProc\":5,\"sourceOutput\":1,\"destProc\":2,\"destInput\":1,\"stats\":[\"batches output: 1\",\"tuples output: 1\",\"execution time: 6.312666s\"]},{\"sourceProc\":5,\"sourceOutput\":1,\"destProc\":10,\"destInput\":1,\"stats\":[\"batches output: 1\",\"tuples output: 3\",\"execution time: 6.312971s\"]},{\"sourceProc\":5,\"sourceOutput\":1,\"destProc\":6,\"destInput\":1,\"stats\":[\"batches output: 0\",\"tuples output: 0\",\"execution time: 6.312112s\",\"max vectorized memory allocated: 2.1 MiB\"]},{\"sourceProc\":6,\"sourceOutput\":0,\"destProc\":7,\"destInput\":0},{\"sourceProc\":7,\"sourceOutput\":0,\"destProc\":12,\"destInput\":1,\"stats\":[\"batches output: 0\",\"tuples output: 0\",\"execution time: 312µs\"]},{\"sourceProc\":8,\"sourceOutput\":0,\"destProc\":9,\"destInput\":0},{\"sourceProc\":9,\"sourceOutput\":1,\"destProc\":2,\"destInput\":1,\"stats\":[\"batches output: 1\",\"tuples output: 1\",\"execution time: 15.423364s\"]},{\"sourceProc\":9,\"sourceOutput\":1,\"destProc\":10,\"destInput\":1,\"stats\":[\"batches output: 1\",\"tuples output: 3\",\"execution time: 15.423313s\",\"max vectorized memory allocated: 2.1 MiB\"]},{\"sourceProc\":9,\"sourceOutput\":1,\"destProc\":6,\"destInput\":1,\"stats\":[\"batches output: 0\",\"tuples output: 0\",\"execution time: 15.423467s\"]},{\"sourceProc\":10,\"sourceOutput\":0,\"destProc\":11,\"destInput\":0},{\"sourceProc\":11,\"sourceOutput\":0,\"destProc\":12,\"destInput\":1,\"stats\":[\"batches output: 1\",\"tuples output: 1\",\"execution time: 177µs\"]},{\"sourceProc\":12,\"sourceOutput\":0,\"destProc\":13,\"destInput\":0}]}"
		//json = strings.TrimSuffix(json, "\n")
		//if (json1 == json) {
		//	fmt.Printf("true\n")
		//}
		//fmt.Printf("%s", json)
		var data bytes.Buffer
		data.Reset()
		data = *bytes.NewBuffer([]byte(json))
		url_test, _ := encoding(data)
		//fmt.Printf("https://cockroachdb.github.io/distsqlplan/decode.html#%s\n",url_test.String())
		fmt.Printf("http://localhost:8089/distsqlplan/decode.html#%s\n", url_test.String())
	}

	//url转json
	fmt.Printf("请输入url地址(#后的数据)：如果不需要可输入回车\n")
	url, _ := reader.ReadString('\n')
	if len(url) > 1 {
		url = "eJzkV9tu20YQfe9XLPYlcrKidvZOAgWY3gCjiVz4gqJoDYMWFzIBiRRIqrAb-LP6A_2ygpSs64aiLCsJkrfhcHd5ZubM2eEHnGax7UdjW-DgTwyYYIYJ5phgga8JnuTZwBZFllevZ4tP43scUIKTdDItK_c1wYMstzj4gMukHFkc4MvodmTPbRTbvEcxwbEto2RUf2KSJ-MofwhHSWqT0o4xwReTKC0C1O11fcY4lyCZ4JQq3xjmY4J_SUalzQMUAqC_ppTywffoFfi-6VLTpfJVEAQ_vb38GRN8btO4XqledyAIgtP-pemG-oSgzqbr5HUnNG-ePCcEhZKgUBEUaoJCn6AQKuS3UTm4swXKpuVkWgaIKV65y-lktOqVSkphMMGnZ6hMxjZAwD2jJTOywFV-Rp5Nh0lqvdxGcYDKfGrx9SPBsxPmaSzKaGhxAI-kfarfDoe5HUZllvf4eqZDRUKNCX7b_-Omf3Z50796964TqpMtl65cF1fvOyFfWGJhwcJilfXj2VX_cr5ybi_XyhXv0r45P_v9onPiyCZsp1Jggu29HUzLJEvnuWQelb42pkrlOLpHf9tBmeXJPzZGYzvO8gcUjUbZICptHCDwNHqf_LCR3WXCbh_QXVTcbeQKSMjw9eOyCOyjRVgeNU2zPLa5jdcOq09pKJNyfXqzJrBdJtZYJrmw1Ny6Oa3KoNeezGKVv-YHuv5Yf37RTUBQyAgKOUGhWOkU3qubRfRCU_VMLwRa9Q20qzQ4Ki1A__dvqzJLSdGvW2Ve1o_v00QXWV7avOdvVuYNCdkbTPC7ZJyU6IC4gLcMC5qjEmtRQXsVZm1VeFWEmW9ASgqs-4WpsGFmO--GaqP9VQ2WnvSF1nCoBO_I80pvy69VgjUojxoYfz4JhheUYPMtSrBjbqGOShvWUqoE30eBd_TQXIFr4HtKcMu46rDa6Sprr6vwHF1dDLddp9p-YWLLpBCOkVcYJkBtjLyCSgXsUL3dkf-VRhZfq94yj4ICAZ9Pb9kL6q3-FvXWUWnuqLQ80si7o4meBJceb-Zte5HsNfNyJy1dpFzE4mRnP-tmkx5szMWHB910y8iPRuIAeG6LSZYWdqPR3CfTKkQbD-0sLUU2zQf2tzwb1L8-s8ezel_tiG1Rzt7C7OE0nb2qAK5uhs3NsLqZrW2GGk0N7NnZk57PFQfdijfMgyfZ2wc10ENgu1p4BlsZVuyLRbWG0nLKmUHRRjugsEYm8GYm8GYaHYcKWoJyBCIascjmQOSnpbTyODClXGE0I3l5mtZQ_Op3eE8oL87SGgnMBsXnt7lqpIFupoF-KT63DJnD0w_IOg7TiMNvDsL_5PIsGOdKOOJohnIUza2wAD-MQ82wjyHPgnGhXPIMO27qXVf11k1zbIXW2sloaL5nYPOiuX787v8AAAD__6fPJl0="
		by, _ := decoding([]byte(url))
		fmt.Printf("json = \n%s\n", by)
	}
}
