package main

import (
	"fmt"
	"unsafe"
)

/*
#include <stdio.h>
#include <stdlib.h>

unsigned char cBytes[16]={'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

static unsigned char *AllocateBytes() {
    printf("--------allocate--------\n");
    for(int i = 0; i < 16; i++){
		printf("cBytes[%d]=%c\t", i, cBytes[i]);
    }
    printf("\n--------%p--------\n", &cBytes);

	return cBytes;
}
*/
import "C"

func main() {
	// 1. 定义声明GO byte[] 切片
	goBytes := make([]byte, 16)
	for i := 0; i < cap(goBytes);  i++{
		goBytes[i] = byte(i)
	}

	// 2. 输出该字节数组
	for _, value := range goBytes {
		fmt.Printf("%b  ", value)
	}
	fmt.Println()

	cBytes := C.AllocateBytes()

	_ = cBytes
	fmt.Printf("address = %v\n", cBytes)
	unsafeBytes := (*[16]byte)(unsafe.Pointer(cBytes))[:]
	for i := 0; i < 16;  i++{
		fmt.Printf("%b  ", unsafeBytes[i])
	}
	fmt.Println()

}