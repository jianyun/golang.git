package main

import (
	"github.com/apache/arrow/go/arrow"
	"github.com/apache/arrow/go/arrow/memory"
)

func readArrowFile() {
	schema := arrow.NewSchema(
		[]arrow.Field{
			arrow.Field{Name: "a", Type: arrow.PrimitiveTypes.Int64,Nullable:true},
			arrow.Field{Name: "b", Type: arrow.PrimitiveTypes.Int64, Nullable: true},
		}, nil,
	)
	mem := memory.NewCheckedAllocator(memory.NewGoAllocator())


	_ = schema
	_ = mem
}
func main() {
	println("------------------arrow go------------------")
}
