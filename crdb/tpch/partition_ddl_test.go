package tpch

import (
	"fmt"
	"testing"
	"time"
)

/**
 * indexName: 索引名称
 * tableName: 表名称
 * partitionColumn: 分区列
 * partitionType: 分区类型, 仅range
 * from, to: 时间起止区间 [from, to)
 * resolution: day month quarter year
 */
func generatePartitionSQL(indexName, tableName, partitionColumn, partitionType, partPrefix string,
	min, max, resolution string) string {
	from, _ := time.Parse(base_format, min)
	to, _ := time.Parse(base_format, max)
	sql := fmt.Sprintf("create index %s on %s(%s) \n"+
		"partition by %s(%s)(\n", indexName, tableName, partitionColumn, partitionType, partitionColumn)
	switch resolution {
	case "day":
		for i, start, end := 0, from, from.AddDate(0, 0, 1); start.Before(to); i, start, end = i+1, start.AddDate(0, 0, 1), end.AddDate(0, 0, 1) {
			part := fmt.Sprintf("    partition %s%d values from('%s') to ('%s'),\n", partPrefix, i, start.Format(base_format), end.Format(base_format))
			sql = sql + part
		}
	case "month":
		for i, start, end := 0, from, from.AddDate(0, 1, 0); start.Before(to); i, start, end = i+1, start.AddDate(0, 1, 0), end.AddDate(0, 1, 0) {
			part := fmt.Sprintf("    partition %s%d values from('%s') to ('%s'),\n", partPrefix, i, start.Format(base_format), end.Format(base_format))
			sql = sql + part
		}
	case "quarter":
		for i, start, end := 0, from, from.AddDate(0, 3, 0); start.Before(to); i, start, end = i+1, start.AddDate(0, 3, 0), end.AddDate(0, 3, 0) {
			part := fmt.Sprintf("    partition %s%d values from('%s') to ('%s'),\n", partPrefix, i, start.Format(base_format), end.Format(base_format))
			sql = sql + part
		}
	case "year":
		for i, start, end := 0, from, from.AddDate(1, 0, 0); start.Before(to); i, start, end = i+1, start.AddDate(1, 0, 0), end.AddDate(1, 0, 0) {
			part := fmt.Sprintf("    partition %s%d values from('%s') to ('%s'),\n", partPrefix, i, start.Format(base_format), end.Format(base_format))
			sql = sql + part
		}
	}

	sql = sql[:len(sql)-2] + ");"
	return sql
}


func TestOrdersPartition(t *testing.T) {
	sql := generatePartitionSQL("o_orderdate_index", "orders", "o_orderdate",
		"range", "orders_part_",
		"1992-01-01", "1998-12-31", "month")

	fmt.Println(sql)

}

// l_shipdate
func TestLineitemPartition(t *testing.T) {
	sql := generatePartitionSQL("l_shipdate_index", "lineitem", "l_shipdate",
		"range", "li_part_",
		"1992-01-01", "1998-12-31", "month")

	fmt.Println(sql)
}