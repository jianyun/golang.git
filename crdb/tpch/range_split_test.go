package tpch

import (
	"database/sql"
	"fmt"
	"log"
	"sort"
	"strconv"
	"testing"
)

var (
	ranges = make(map[int]PartitionRange)
)

type RangePretty struct {
	start string
	end string
}

func getTableId(database, table string) int {
	db, err := sql.Open("postgres", dataSource)
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}
	defer db.Close()

	sql := fmt.Sprintf("select table_id from %s.tables\nwhere state='PUBLIC' and name='%s' and database_name='%s'", internal, table, database)

	rows, err := db.Query(sql)
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	defer rows.Close()

	for rows.Next() {
		var tableId     int
		if err := rows.Scan(&tableId); err != nil {
			log.Printf("%s\n", err)
		}

		return tableId
	}

	return 0
}

func splitRanges(database, table string) {
	tableId := getTableId(database, table)
	if tableId == 0 {return}

	db, err := sql.Open("postgres", dataSource)
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}
	defer db.Close()
	startKey := "/Table/"+strconv.Itoa(tableId)

	sql := fmt.Sprintf("select start_pretty, end_pretty from %s.ranges\n where start_pretty >= '%s' and index_name ='' and schema_name='public' and database_name='%s' and table_name='%s'", internal, startKey, database, table)

	rows, err := db.Query(sql)
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	defer rows.Close()

	for rows.Next() {
		var (
			startPretty string
			endPretty string
		)
		if err := rows.Scan(&startPretty, &endPretty); err != nil {
			log.Printf("%s\n", err)
		}

		rp := RangePretty{startPretty, endPretty}
		_ = rp
		fmt.Println(rp)
	}

}

func TestLineitemSplitRanges(t *testing.T)  {
	db, err := sql.Open("postgres", dataSource)
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}
	defer db.Close()
	sql := fmt.Sprintf("select * from (select t.l_partition, t.l_orderkey, min(l_linenumber) l_linenumber\n"  +
		"from tpchp.public.lineitem l join\n" +
		"(\n" +
		"    select l_partition, min(l_orderkey) l_orderkey \n" +
		"    from tpchp.public.lineitem\n" +
		"    group by l_partition\n " +
		") t on t.l_partition=l.l_partition and  l.l_orderkey= t.l_orderkey \n" +
		"group by t.l_partition, t.l_orderkey \n" +
		"union \n" +
		"select t.l_partition, t.l_orderkey, max(l_linenumber) l_linenumber\n"  +
		"from tpchp.public.lineitem l join\n" +
		"(\n" +
		"    select l_partition, max(l_orderkey) l_orderkey \n" +
		"    from tpchp.public.lineitem\n" +
		"    group by l_partition\n " +
		") t on t.l_partition=l.l_partition and  l.l_orderkey= t.l_orderkey \n" +
		"group by t.l_partition, t.l_orderkey) \n" +
		"order by l_partition, l_orderkey")
	println(sql)

	rows, err := db.Query(sql)
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	defer rows.Close()

	type IntPair struct {
		orderkey int
		linenumber int
	}

	type PartKeyRange struct {
		start IntPair
		end IntPair
	}
	
	partKeyRanges := make(map[int]PartKeyRange, len(partIds))
	partitionIds := make([]int, 0)
	for rows.Next() {
		var (
			partition int
			orderkey int
			linenumber int
		)
		if err := rows.Scan(&partition, &orderkey, &linenumber); err != nil {
			log.Printf("%s\n", err)
		}

		fmt.Printf("%d\t%d\t%d\n", partition, orderkey, linenumber)
		pkr, exists := partKeyRanges[partition]
		if exists {
			pkr.end = IntPair{
				orderkey:   orderkey,
				linenumber: linenumber,
			}
			partKeyRanges[partition] = pkr
		} else {
			partitionIds = append(partitionIds, partition)
			partKeyRanges[partition] = PartKeyRange{
				start:	IntPair{orderkey:orderkey, linenumber:linenumber},
			}
		}
	}

	sort.Ints(partitionIds)

	//alterSQL := "ALTER TABLE users SPLIT AT VALUES ('chicago'), ('new york'), ('seattle');"

	alterSQL := "ALTER TABLE lineitem SPLIT AT VALUES "

	for _, partitionId := range partitionIds {
		pkr := partKeyRanges[partitionId]

		fmt.Printf("%d -> %v \n", partitionId, pkr)
		alterSQL = alterSQL + fmt.Sprintf("(%d, %d, %d), ", partitionId, pkr.start.orderkey, pkr.end.linenumber)
	}

	alterSQL = alterSQL[:len(alterSQL)-2] + ";"
	// SET CLUSTER SETTING kv.range_merge.queue_enabled = false;
	fmt.Printf("%s\n", alterSQL)
	//splitRanges(database, table)

}

func TestOrdersSplitRanges(t *testing.T)  {
	db, err := sql.Open("postgres", dataSource)
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}
	defer db.Close()
	sql := fmt.Sprintf("select o_partition, min(o_orderkey), max(o_orderkey) \n" +
		"from orders \n" +
		"group by o_partition \n" +
		"order by o_partition")
	println(sql)

	rows, err := db.Query(sql)
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	defer rows.Close()

	type OrderKeyRange struct {
		start int
		end int
	}
	orderKeyRanges := make(map[int]OrderKeyRange, len(partIds))
	partitionIds := make([]int, 0)
	for rows.Next() {
		var (
			partition int
			min int
			max int
		)
		if err := rows.Scan(&partition, &min, &max); err != nil {
			log.Printf("%s\n", err)
		}

		fmt.Printf("%d\t%d\t%d\n", partition, min, max)
		_, exists := orderKeyRanges[partition]
		if !exists {
			partitionIds = append(partitionIds, partition)
			orderKeyRanges[partition] = OrderKeyRange{
				start:	min,
				end: max,
			}
		}
	}

	sort.Ints(partitionIds)

	//alterSQL := "ALTER TABLE users SPLIT AT VALUES ('chicago'), ('new york'), ('seattle');"

	alterSQL := "ALTER TABLE orders SPLIT AT VALUES "

	for _, partitionId := range partitionIds {
		okr := orderKeyRanges[partitionId]

		fmt.Printf("%d -> %v \n", partitionId, okr)
		alterSQL = alterSQL + fmt.Sprintf("(%d, %d), ", partitionId, okr.start)
	}

	alterSQL = alterSQL[:len(alterSQL)-2] + ";"
	// SET CLUSTER SETTING kv.range_merge.queue_enabled = false;
	fmt.Printf("%s\n", alterSQL)
	//splitRanges(database, table)

}

/**
		sql := fmt.Sprintf("select min(l_orderkey), min(l_linenumber), max(l_orderkey), max(l_linenumber) from %s.public.lineitem where l_partition=%d", database, partitionId)
		fmt.Printf("%s\n", sql)

startSQL := fmt.Sprintf("select t.l_partition, t.l_orderkey, min(l_linenumber) l_linenumber\n"  +
		"from tpchp.public.lineitem l join\n" +
		"(\n" +
		"    select l_partition, min(l_orderkey) l_orderkey \n" +
		"    from tpchp.public.lineitem\n" +
		"    group by l_partition\n " +
		") t on t.l_partition=l.l_partition and  l.l_orderkey= t.l_orderkey \n" +
		"group by t.l_partition, t.l_orderkey \n" +
		"order by t.l_partition")
	println(startSQL)

	endSQL := fmt.Sprintf("select t.l_partition, t.l_orderkey, max(l_linenumber) l_linenumber\n"  +
		"from tpchp.public.lineitem l join\n" +
		"(\n" +
		"    select l_partition, max(l_orderkey) l_orderkey \n" +
		"    from tpchp.public.lineitem\n" +
		"    group by l_partition\n " +
		") t on t.l_partition=l.l_partition and  l.l_orderkey= t.l_orderkey \n" +
		"group by t.l_partition, t.l_orderkey \n" +
		"order by t.l_partition")
	println(endSQL)
 */