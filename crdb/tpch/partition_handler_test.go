package tpch

import (
	"bufio"
	"io"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	order_csv_file      = "/opt/tpc-h-2.18.0_rc2/tables/orders.tbl"
	order_csv_file_part = "/opt/tpc-h-2.18.0_rc2/tables/orders_part.tbl"

	li_csv_file      = "/opt/tpc-h-2.18.0_rc2/tables/lineitem.tbl"
	li_csv_file_part = "/opt/tpc-h-2.18.0_rc2/tables/lineitem_part.tbl"

	batch_size = 5000
)

func AddPartition2Csv(src, dest, delimiter string, partitionIndex int) {
	// source source
	source, err := os.Open(src)
	if err != nil {
		panic(err)
	}
	defer source.Close()

	// writer file
	fw, err := os.OpenFile(dest, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		panic(err)
	}
	defer fw.Close()

	writer := bufio.NewWriter(fw)
	reader := bufio.NewReader(source)
	count := 0
	for {
		line, _, err := reader.ReadLine()
		if err == io.EOF {
			break
		}

		count++

		row := AddPartition(string(line), delimiter, partitionIndex)
		println(row)

		writer.WriteString(row + "\r\n")
		if count%batch_size == 0 {
			writer.Flush()
		}
	}

	writer.Flush()
}

func AddPartition(line, delimiter string, partitionIndex int) string {
	columns := strings.Split(line, delimiter)
	if len(columns) > partitionIndex+1 {
		column := columns[partitionIndex]
		for _, partitionId := range partIds {
			partition := partitions[partitionId]
			date, _ := time.Parse(base_format, column)
			if date.Before(partition.to) && (date.After(partition.from) || date.Equal(partition.from)) {
				line = line + strconv.Itoa(partitionId) + delimiter
			}
		}
	}

	return line
}

//func TestOrdersAddPartition(t *testing.T) {
//	AddPartition2Csv(order_csv_file,  order_csv_file_part, "|", 4)
//}

//func TestLineitemAddPartition(t *testing.T) {
//	AddPartition2Csv(li_csv_file,  li_csv_file_part, "|", 10)
//}
