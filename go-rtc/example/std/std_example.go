package main

import (
	"context"
	"fmt"
	"go-rtc/common"
	"go-rtc/std"
	"time"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	ch := make(chan uintptr)
	defer close(ch)
	source := std.NewStdinSource(ctx)
	sink := std.NewStdoutSink(ctx)

	source.AddOutput(ch)
	sink.AddInput(ch)

	source.Init(common.Options{})
	sink.Init(common.Options{})
	go func() {
		source.Read()
	}()

	go func() {
		sink.Write()
	}()

	select {
	case <-time.After(60 * time.Second):
		fmt.Println("Timeout exit")
		cancel()
	case <-ctx.Done():
		fmt.Println("continue")
	}
	source.Close()
	sink.Close()
}
