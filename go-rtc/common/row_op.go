package common

// 一元运算
type UnaryOperator = func(IN any) (err error)

// 二元运算
type BinaryOperator = func(IN1 any, IN2 any) (OUT any, err error)

// 三参数运算
type ThirdOperator = func(IN1 any, IN2 any, IN3 any) (OUT any, err error)

const (
	Unary = iota
	Binary
	Third
)

type Operators struct {
	chains map[int]int
	unary  []UnaryOperator
	binary []BinaryOperator
	third  []ThirdOperator
}
