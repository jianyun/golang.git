package common

type Options map[string]string

type Source interface {
	Init(options Options) error
	SetOutputs(ch []*chan any)
	AddOutput(ch chan any)
	Read() error
	Close() error
}

type Sink interface {
	Init(options Options) error
	SetInputs(ch []*chan any)
	AddInput(ch chan any)
	Write() error
	Close() error
}

type Sources []Source

type Sinks []Sink
