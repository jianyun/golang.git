package base

import (
	"fmt"
	"testing"
)

func TestSliceAppend(t *testing.T) {
	slice := []string{"hello"}
	_slice := append(slice, "world", "!")
	for i, word := range _slice {
		fmt.Printf("slice[%d]=%s\n", i, word)
	}

	slice = append([]string{""}, slice...)
	for i, word := range slice {
		fmt.Printf("slice[%d]=%s\n", i, word)
	}
}

func TestDimenAppend(t *testing.T) {
	//var array1 [][]string
	array1 := [][]string{{"000", "111"}}
	var tmp []string

	tmp = append(tmp, "001")
	tmp = append(tmp, "002")

	array1 = append(array1, tmp)

	fmt.Println(array1)
	fmt.Println(array1[1])
	fmt.Println(array1[1][1])

	array2 := [][]string{{"aaa", "bbb"}}
	array2 = append(array2, array1[0])
	array2 = append(array2, array1[1])
	fmt.Println(array2)
}

func TestRequestUnionSliceAppend(t *testing.T) {
	slice := []RequestUnion{{"hello"}, {"word"}}
	fmt.Printf("slice len=%d, cap=%d, addr=%p\n", len(slice), cap(slice), &slice)
	slice = append([]RequestUnion(nil), slice...)
	fmt.Printf("slice len=%d, cap=%d, addr=%p\n", len(slice), cap(slice), &slice)
	_slice := append([]RequestUnion(nil), slice...)
	fmt.Printf("slice len=%d, cap=%d, addr=%p\n", len(_slice), cap(_slice), &_slice)
	slice[len(slice)-1].SetInner("world")
	for i, _ := range slice {
		fmt.Printf("slice[%d]=%s\n", i, slice[i])
	}
}

func TestVecScanResponse_Combine(t *testing.T) {
	cases := []struct {
		desc   string
		vsr    VecScanResponse
		oth    VecScanResponse
		result VecScanResponse
	}{
		{
			desc: "len(vsr.ColBatch) == len(oth.ColBatch)",
			vsr: VecScanResponse{
				[]VecValue{
					{1, 1, [][]uint32{{0, 0, 0}, {1, 1, 1}}},
					{2, 1, [][]uint32{{1, 1, 1}, {2, 2, 2}}},
					{3, 1, [][]uint32{{2, 2, 2}, {3, 3, 3}}},
				},
			},
			oth: VecScanResponse{
				[]VecValue{
					{1, 1, [][]uint32{{2, 2, 2}, {3, 3, 3}}},
					{2, 1, [][]uint32{{3, 3, 3}, {4, 4, 4}}},
					{3, 1, [][]uint32{{4, 4, 4}, {5, 5, 5}}},
				},
			},
			result: VecScanResponse{
				[]VecValue{
					{1, 1, [][]uint32{{0, 0, 0}, {1, 1, 1}, {2, 2, 2}, {3, 3, 3}}},
					{2, 1, [][]uint32{{1, 1, 1}, {2, 2, 2}, {3, 3, 3}, {4, 4, 4}}},
					{3, 1, [][]uint32{{2, 2, 2}, {3, 3, 3}, {4, 4, 4}, {5, 5, 5}}},
				},
			},
		},
	}

	for i, tc := range cases {
		err := tc.vsr.Combine(tc.oth)
		if err != nil {
			fmt.Errorf("test case %d error %s\n", i, err)
		}

		for i, cb := range tc.vsr.ColBatch {
			fmt.Printf("colbatch[%d]=%v\n", i, cb)
		}
	}
}
