grammar LabeledExpr; // 为了和原先的语法区分开，进行了重命名

import CommonLexerRules; // 引入CommonLexerRules.g4中的全部规则

/** 其实规则，语法分析的起点 */
prog : stat+ ;

stat : expr NEWLINE  #printExpr
     | ID '=' expr NEWLINE # assign
     | NEWLINE # blank
     | CLEAR # clear
     ;

expr : expr op=('*'|'/') expr # MulDiv
     | expr op=('+'|'-') expr # AddSub
     | INT # int
     | ID # id
     | '(' expr ')' # parens
     ;

MUL : '*' ; // 为上述语法中使用的'*'命名
DIV : '/' ; 
ADD : '+' ; 
SUB : '-' ; 

CLEAR : 'clear' ; // 清空内存

// export CLASSPATH="$CLASSPATH:.:/usr/local/antlr/antlr-4.10.1-complete.jar"
// antlr4 -Dlanguage=Java -no-listener -visitor -o java/lblexpr LabeledExpr.g4

// antlr4 -Dlanguage=Go -no-listener -visitor -o go/parser LabeledExpr.g4