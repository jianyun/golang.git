grammar Hello; // 定义一个名为Hello的语法
r : 'hello' ID; // 匹配一个关键hello和一个紧随其后的标识符
ID : [a-z]+ ; // 匹配小写字母组成的标识符
WS : [ \t\r\n]+ -> skip; //忽略空格、Tab、换行以及\r(Windows)


// antlr4 -Dlanguage=Go -o hello Hello.g4

// export CLASSPATH="$CLASSPATH:.:/usr/local/antlr/antlr-4.10.1-complete.jar"
// antlr4 -Dlanguage=Java -o hello Hello.g4
// cd Hello && javac *.java
// grun Hello r -tokens