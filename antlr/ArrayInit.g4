/**
 * 语法文件通常以grammar关键字开头
 * 这是一个名为ArrayInit的语法，它必须与文件名ArrayInit.g4相匹配
 */
 grammar ArrayInit;

 /** 一条名为init的规则，它匹配一对花括号中的、逗号分隔的value */
 init : '{' value (',' value)* '}'; // 必须匹配至少一个value

 /** 一个value可以是嵌套的花货号结构，也可以是一个简单的证书，即INT词法符号 */
 value : init
       | INT
       ;

// 语法分析器的规则必须以小写字母开头，词法分析器的规则必须用大写字母开发
INT: [0-9]+ ; // 定义词法符号INT,它由一个或多个数字组成
WS: [ \t\r\n]+ -> skip; // 定义词法规则 "空白符号", 丢弃之


// export CLASSPATH="$CLASSPATH:.:/usr/local/antlr/antlr-4.10.1-complete.jar"
// antlr4 -Dlanguage=Java -o java/array ArrayInit.g4
// grun ArrayInit init -gui
// grun ArrayInit init -tree
// grun ArrayInit init -tokens
// antlr4 -Dlanguage=Go -o go/parser ArrayInit.g4