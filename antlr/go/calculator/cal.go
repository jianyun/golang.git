package main

import (
	"fmt"
	"github.com/antlr/antlr4/runtime/Go/antlr"
	"parser"
)

func NewBaseLabeledExprVisitor() *parser.BaseLabeledExprVisitor {
	return &parser.BaseLabeledExprVisitor{
		BaseParseTreeVisitor: new(antlr.BaseParseTreeVisitor),
	}
}
func NewEvalVisitor() *EvalVisitor {
	return &EvalVisitor{
		BaseLabeledExprVisitor: NewBaseLabeledExprVisitor(),
		memory:                 make(map[string]interface{}, 0),
	}
}

func main() {
	// input, _ := antlr.NewFileStream(os.Stdin)
	input, _ := antlr.NewFileStream("t.expr")
	//input := antlr.NewInputStream("{1, 2, 3}")
	lexer := parser.NewLabeledExprLexer(input)
	tokens := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
	p := parser.NewLabeledExprParser(tokens)
	p.AddErrorListener(antlr.NewDiagnosticErrorListener(true))
	p.BuildParseTrees = true
	tree := p.Prog()

	eval := NewEvalVisitor()

	result := eval.Visit(tree) // tree.Accept(eval)
	if nil != result {
		fmt.Println(result)
	}
}
