// Code generated from LabeledExpr.g4 by ANTLR 4.10.1. DO NOT EDIT.

package parser

import (
	"fmt"
	"sync"
	"unicode"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import error
var _ = fmt.Printf
var _ = sync.Once{}
var _ = unicode.IsLetter

type LabeledExprLexer struct {
	*antlr.BaseLexer
	channelNames []string
	modeNames    []string
	// TODO: EOF string
}

var labeledexprlexerLexerStaticData struct {
	once                   sync.Once
	serializedATN          []int32
	channelNames           []string
	modeNames              []string
	literalNames           []string
	symbolicNames          []string
	ruleNames              []string
	predictionContextCache *antlr.PredictionContextCache
	atn                    *antlr.ATN
	decisionToDFA          []*antlr.DFA
}

func labeledexprlexerLexerInit() {
	staticData := &labeledexprlexerLexerStaticData
	staticData.channelNames = []string{
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN",
	}
	staticData.modeNames = []string{
		"DEFAULT_MODE",
	}
	staticData.literalNames = []string{
		"", "'='", "'('", "')'", "'*'", "'/'", "'+'", "'-'", "'clear'",
	}
	staticData.symbolicNames = []string{
		"", "", "", "", "MUL", "DIV", "ADD", "SUB", "CLEAR", "ID", "INT", "NEWLINE",
		"WS",
	}
	staticData.ruleNames = []string{
		"T__0", "T__1", "T__2", "MUL", "DIV", "ADD", "SUB", "CLEAR", "ID", "INT",
		"NEWLINE", "WS",
	}
	staticData.predictionContextCache = antlr.NewPredictionContextCache()
	staticData.serializedATN = []int32{
		4, 0, 12, 67, 6, -1, 2, 0, 7, 0, 2, 1, 7, 1, 2, 2, 7, 2, 2, 3, 7, 3, 2,
		4, 7, 4, 2, 5, 7, 5, 2, 6, 7, 6, 2, 7, 7, 7, 2, 8, 7, 8, 2, 9, 7, 9, 2,
		10, 7, 10, 2, 11, 7, 11, 1, 0, 1, 0, 1, 1, 1, 1, 1, 2, 1, 2, 1, 3, 1, 3,
		1, 4, 1, 4, 1, 5, 1, 5, 1, 6, 1, 6, 1, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 7,
		1, 8, 4, 8, 47, 8, 8, 11, 8, 12, 8, 48, 1, 9, 4, 9, 52, 8, 9, 11, 9, 12,
		9, 53, 1, 10, 3, 10, 57, 8, 10, 1, 10, 1, 10, 1, 11, 4, 11, 62, 8, 11,
		11, 11, 12, 11, 63, 1, 11, 1, 11, 0, 0, 12, 1, 1, 3, 2, 5, 3, 7, 4, 9,
		5, 11, 6, 13, 7, 15, 8, 17, 9, 19, 10, 21, 11, 23, 12, 1, 0, 3, 2, 0, 65,
		90, 97, 122, 1, 0, 48, 57, 2, 0, 9, 9, 32, 32, 70, 0, 1, 1, 0, 0, 0, 0,
		3, 1, 0, 0, 0, 0, 5, 1, 0, 0, 0, 0, 7, 1, 0, 0, 0, 0, 9, 1, 0, 0, 0, 0,
		11, 1, 0, 0, 0, 0, 13, 1, 0, 0, 0, 0, 15, 1, 0, 0, 0, 0, 17, 1, 0, 0, 0,
		0, 19, 1, 0, 0, 0, 0, 21, 1, 0, 0, 0, 0, 23, 1, 0, 0, 0, 1, 25, 1, 0, 0,
		0, 3, 27, 1, 0, 0, 0, 5, 29, 1, 0, 0, 0, 7, 31, 1, 0, 0, 0, 9, 33, 1, 0,
		0, 0, 11, 35, 1, 0, 0, 0, 13, 37, 1, 0, 0, 0, 15, 39, 1, 0, 0, 0, 17, 46,
		1, 0, 0, 0, 19, 51, 1, 0, 0, 0, 21, 56, 1, 0, 0, 0, 23, 61, 1, 0, 0, 0,
		25, 26, 5, 61, 0, 0, 26, 2, 1, 0, 0, 0, 27, 28, 5, 40, 0, 0, 28, 4, 1,
		0, 0, 0, 29, 30, 5, 41, 0, 0, 30, 6, 1, 0, 0, 0, 31, 32, 5, 42, 0, 0, 32,
		8, 1, 0, 0, 0, 33, 34, 5, 47, 0, 0, 34, 10, 1, 0, 0, 0, 35, 36, 5, 43,
		0, 0, 36, 12, 1, 0, 0, 0, 37, 38, 5, 45, 0, 0, 38, 14, 1, 0, 0, 0, 39,
		40, 5, 99, 0, 0, 40, 41, 5, 108, 0, 0, 41, 42, 5, 101, 0, 0, 42, 43, 5,
		97, 0, 0, 43, 44, 5, 114, 0, 0, 44, 16, 1, 0, 0, 0, 45, 47, 7, 0, 0, 0,
		46, 45, 1, 0, 0, 0, 47, 48, 1, 0, 0, 0, 48, 46, 1, 0, 0, 0, 48, 49, 1,
		0, 0, 0, 49, 18, 1, 0, 0, 0, 50, 52, 7, 1, 0, 0, 51, 50, 1, 0, 0, 0, 52,
		53, 1, 0, 0, 0, 53, 51, 1, 0, 0, 0, 53, 54, 1, 0, 0, 0, 54, 20, 1, 0, 0,
		0, 55, 57, 5, 13, 0, 0, 56, 55, 1, 0, 0, 0, 56, 57, 1, 0, 0, 0, 57, 58,
		1, 0, 0, 0, 58, 59, 5, 10, 0, 0, 59, 22, 1, 0, 0, 0, 60, 62, 7, 2, 0, 0,
		61, 60, 1, 0, 0, 0, 62, 63, 1, 0, 0, 0, 63, 61, 1, 0, 0, 0, 63, 64, 1,
		0, 0, 0, 64, 65, 1, 0, 0, 0, 65, 66, 6, 11, 0, 0, 66, 24, 1, 0, 0, 0, 5,
		0, 48, 53, 56, 63, 1, 6, 0, 0,
	}
	deserializer := antlr.NewATNDeserializer(nil)
	staticData.atn = deserializer.Deserialize(staticData.serializedATN)
	atn := staticData.atn
	staticData.decisionToDFA = make([]*antlr.DFA, len(atn.DecisionToState))
	decisionToDFA := staticData.decisionToDFA
	for index, state := range atn.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(state, index)
	}
}

// LabeledExprLexerInit initializes any static state used to implement LabeledExprLexer. By default the
// static state used to implement the lexer is lazily initialized during the first call to
// NewLabeledExprLexer(). You can call this function if you wish to initialize the static state ahead
// of time.
func LabeledExprLexerInit() {
	staticData := &labeledexprlexerLexerStaticData
	staticData.once.Do(labeledexprlexerLexerInit)
}

// NewLabeledExprLexer produces a new lexer instance for the optional input antlr.CharStream.
func NewLabeledExprLexer(input antlr.CharStream) *LabeledExprLexer {
	LabeledExprLexerInit()
	l := new(LabeledExprLexer)
	l.BaseLexer = antlr.NewBaseLexer(input)
	staticData := &labeledexprlexerLexerStaticData
	l.Interpreter = antlr.NewLexerATNSimulator(l, staticData.atn, staticData.decisionToDFA, staticData.predictionContextCache)
	l.channelNames = staticData.channelNames
	l.modeNames = staticData.modeNames
	l.RuleNames = staticData.ruleNames
	l.LiteralNames = staticData.literalNames
	l.SymbolicNames = staticData.symbolicNames
	l.GrammarFileName = "LabeledExpr.g4"
	// TODO: l.EOF = antlr.TokenEOF

	return l
}

// LabeledExprLexer tokens.
const (
	LabeledExprLexerT__0    = 1
	LabeledExprLexerT__1    = 2
	LabeledExprLexerT__2    = 3
	LabeledExprLexerMUL     = 4
	LabeledExprLexerDIV     = 5
	LabeledExprLexerADD     = 6
	LabeledExprLexerSUB     = 7
	LabeledExprLexerCLEAR   = 8
	LabeledExprLexerID      = 9
	LabeledExprLexerINT     = 10
	LabeledExprLexerNEWLINE = 11
	LabeledExprLexerWS      = 12
)
