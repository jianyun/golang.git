package main

import (
	"fmt"
	"parser"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

type ShortToUnicodeStringListener struct {
	parser.BaseArrayInitListener
}

func NewShortToUnicodeStringListener() *ShortToUnicodeStringListener {
	return new(ShortToUnicodeStringListener)
}

// EnterInit is called when production init is entered.
func (s *ShortToUnicodeStringListener) EnterInit(ctx *parser.InitContext) {
	fmt.Print("\"")
}

// ExitInit is called when production init is exited.
func (s *ShortToUnicodeStringListener) ExitInit(ctx *parser.InitContext) {
	fmt.Print("\"")
}

// EnterValue is called when production value is entered.
func (s *ShortToUnicodeStringListener) EnterValue(ctx *parser.ValueContext) {
	value, _ := strconv.Atoi(ctx.INT().GetText())
	fmt.Printf("\\u%04X", value)
}

func main() {
	// input, _ := antlr.NewFileStream(os.Stdin)
	input := antlr.NewInputStream("{1, 2, 3}")
	lexer := parser.NewArrayInitLexer(input)
	stream := antlr.NewCommonTokenStream(lexer, 0)
	p := parser.NewArrayInitParser(stream)
	p.AddErrorListener(antlr.NewDiagnosticErrorListener(true))
	p.BuildParseTrees = true
	tree := p.Init()
	antlr.ParseTreeWalkerDefault.Walk(NewShortToUnicodeStringListener(), tree)

	// 翻译完成后换行
	fmt.Println()
}
