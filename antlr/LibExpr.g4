grammar LibExpr; // 为了和原先的语法区分开，进行了重命名

import CommonLexerRules; // 引入CommonLexerRules.g4中的全部规则

/** 其实规则，语法分析的起点 */
prog : stat+ ;

stat : expr NEWLINE
     | ID '=' expr NEWLINE
     | NEWLINE
     ;

expr : expr ('*'|'/') expr
     | expr ('+'|'-') expr
     | INT
     | ID
     | '(' expr ')'
     ;

// export CLASSPATH="$CLASSPATH:.:/usr/local/antlr/antlr-4.10.1-complete.jar"
// antlr4 -Dlanguage=Java -o java/libexpr LibExpr.g4

// antlr4 -Dlanguage=Go -o go/parser LibExpr.g4

// grun Expr stat -gui