package golang

const (
	GOPATH = "/go"
	GOMOD  = GOPATH + "/pkg/mod"
	GITHUB = GOMOD + "/github.com"

	_ = GITHUB
)
